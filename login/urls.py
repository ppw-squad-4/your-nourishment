from django.conf.urls import url
from . import views
from django.shortcuts import render
from django.urls import path
from django.contrib import admin
from .views import add_login

app_name = 'login'
urlpatterns = [
    path('add_login/', views.add_login, name = 'add_login'),
]
