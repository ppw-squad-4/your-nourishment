from django.db import models
from django.utils import timezone
from datetime import datetime, date
from django.core.validators import MaxValueValidator, MinValueValidator

class Testimoni(models.Model):
    nama = models.CharField(max_length=100)
    usia = models.PositiveIntegerField(default=17,
        	validators=[
            MaxValueValidator(100),
            MinValueValidator(0)
            ])
    pesan = models.TextField()


# Create your models here.
