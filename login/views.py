from django.shortcuts import render
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth import authenticate

# Create your views here.

def add_login(request):
    if request.method == "POST":
        user = authenticate (username = request.POST['username'], password = request.POST['password'])
        if user is not None:
            login(request, user)      
            return redirect(reverse('login:add_login'))
    else:
        messages.add_message(request, messages.ERROR, 'Error account')
    return render(request, 'login.html')
