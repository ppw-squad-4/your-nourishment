from django.urls import path

from . import views

from .views import *


app_name = 'home'

urlpatterns = [

    path('', views.homepage, name='homepage'),
    path('base/', views.base, name='base'),
    path('create_testi/', views.create_testi, name='create_testi'),
    path('testi/', views.testi, name='testi'),

    path('', homepage, name='homepage'),
    path('base/', base, name='base'),

    # dilanjutkan ...
]
