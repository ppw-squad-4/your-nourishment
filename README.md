# Your Nourishment

**KC 04 PPW Group Project**


## Group Member

* Hans Michael Nabasa Pasaribu - 1806235662
* Irham Ilman Zhafir - 1806191061
* Maisy Rahmawati - 1806147035
* Nicolas Henry Wijaya - 1806191566

## Herokuapp Link

[Your Nourishment](https://yournourishment.herokuapp.com) 

## Project Details

Your Nourishment adalah aplikasi pemberi saran pemenuhan gizi makanan serta aktivitas kepada para penggunanya agar mereka tetap berada pada kondisi yang prima dan bugar. 

Pada penggunaannya, aplikasi ini akan meminta data-data kesehatan dari pengguna seperti tinggi badan, berat badan, tekanan darah, kondisi alergi, makanan yang dikonsumsi. Semua data-data ini akan diolah melalui sistem sehingga menghasilkan _output_ berupa rekomendasi makanan dan aktivitas untuk menjaga kesehatan dan kebugaran pengguna sesuai dengan kondisi data-data yang diberikan. Selain dari fitur utama tersebut, aplikasi ini juga memiliki fitur-fitur lain seperti testimoni dari pengguna, artikel yang berisikan info-info kesehatan, serta fitur login. 
Aplikasi ini dapat dikatakan sebagai _personal assistant_ khusus dalam hal kesehatan bagi pengguna, sehingga walaupun pengguna adalah orang yang sibuk, ia tetap bisa me-_maintain_ kesehatannya melalui aplikasi ini. 

## Features

- Fitur utama 

Fitur utama dari aplikasi ini adalah memberikan info mengenai kondisi pengguna saat ini kemudian merekomendasikan kepada pengguna mengenai makanan dan aktivitas yang diperlukan pengguna untuk tetap sehat dan optimal melalui serangkaian pertanyaan / data mengenai keadaan pengguna sekarang seperti, berat badan, tekanan darah, makanan yang baru dikonsumsi, dan lain-lain. 
- Testimoni

Fitur testimoni ini akan menampilkan testimoni / review dari para pengguna
- Artikel kesehatan

Fitur Artikel akan menyediakan berbagai macam artikel mulai dari tips kesehatan hingga saran makanan dan aktivitas.
- Fitur login 

Fitur ini mengizinkan pengguna untuk menyimpan data pengguna serta menjaga privasi dari para pengguna lain.

## Pipelines Status

[![pipeline status](https://gitlab.com/ppw-squad-4/your-nourishment/badges/master/pipeline.svg)](https://gitlab.com/ppw-squad-4/your-nourishment/commits/master)