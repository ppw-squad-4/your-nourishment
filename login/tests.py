from django.test import TestCase, Client
from django .http import HttpRequest
from .views import add_login

# Create your tests here.
class LoginUnitTest(TestCase):

    #Mengecek alamat url telah ada
    def test_add_login_url_exist(self):
        response = Client().get('/add_login/')
        self.assertEqual(response.status_code, 200)

    #Mengecek apakah template telah ada
    def test_add_login_template_exist(self):
        response = Client().get('/add_login/')
        self.assertTemplateUsed(response, 'login.html')

    #Mengecek content username telah ada di views
    # test denied because it was unfinished
    # def test_username_is_written(self):
    #     self.assertIsNotNone(user)
