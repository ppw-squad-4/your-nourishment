from . import models
from django import forms
from datetime import datetime, date

class TestimoniForm(forms.Form):
	nama = forms.CharField(
		widget=forms.TextInput(attrs={'required' : 'True', 'class':'form-control'})
							)
	usia = forms.IntegerField()
				
	pesan = forms.CharField(
		widget=forms.Textarea(attrs={'required' : 'True', 'class':'form-control'})
							)

