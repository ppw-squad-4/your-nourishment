from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class CustomUser(AbstractUser):
    GENDER=[('Laki-Laki','Laki-Laki'), ('Perempuan','Perempuan')]
    initial_name = models.CharField(max_length=100)
    gender = models.CharField(max_length=100, choices=GENDER)
