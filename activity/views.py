from django.shortcuts import render, HttpResponseRedirect
from django.http import HttpResponse

# Create your views here.
def index(request):
    context = {
        "empty": 0,
        "uploaded" : 0,
        "age_val":"",
        "weight_val":"",
        "height_val":"",
        "food_val":"",
        "sleep_val":"",
    }
    if(request.method == "POST"):
        age = request.POST["age"]
        gender = request.POST["gender"]
        weight = request.POST["weight"]
        height = request.POST["height"]
        activeness = request.POST["activeness"]
        last_food = request.POST["last-food"]
        sleep_time = request.POST["sleep-time"]

        context["age_vale"] = age
        context["weight_val"] = weight
        context["height_val"] = height
        context["food_val"] = last_food
        context["sleep_val"] = sleep_time

        context["uploaded"] = 1
        if(age == ""):
            context["empty"] = 0
        elif(weight == ""):
            context["empty"] = 2
        elif(height == ""):
            context["empty"] = 3
        elif(last_food == ""):
            context["empty"] = 5
        elif(sleep_time == ""):
            context["empty"] = 6
        else:
            cleaned_height = height.strip('c').strip('m')
            cleaned_weight = weight.strip('k').strip('g')
            if(gender == "male"):
                energy = round((66.5 + 13.8 * int(cleaned_weight) + 5 * int(cleaned_height)) - (6.8 * int(age)),2)
            else:
                energy = round((655.1 + 9.6 * int(cleaned_weight) + 1.9 * int(cleaned_height)) - (4.7 * int(age)),2)

            if(activeness == "rendah"):
                energy *= 1.2
            elif(activeness == "sedang"):
                energy *= 1.3
            else:
                energy *= 1.5

            context = {
                "empty": 0,
                "uploaded" : 0,
                "age_val":"",
                "weight_val":"",
                "height_val":"",
                "food_val":"",
                "sleep_val":"",
            }

            context["energy"] = energy

            request.session['post'] = energy

            return HttpResponseRedirect('../result')

    return render(request, 'activity.html', context)

def result(request):
    # context = {
    #     "empty": 0,
    #     "uploaded" : 0,
    #     "age_val":"",
    #     "weight_val":"",
    #     "height_val":"",
    #     "food_val":"",
    #     "sleep_val":"",
    # }
    # if(request.method == "POST"):
    #     age = request.POST["age"]
    #     gender = request.POST["gender"]
    #     weight = request.POST["weight"]
    #     height = request.POST["height"]
    #     activeness = request.POST["activeness"]
    #     last_food = request.POST["last-food"]
    #     sleep_time = request.POST["sleep-time"]
    #
    #     context["age_vale"] = age
    #     context["weight_val"] = weight
    #     context["height_val"] = height
    #     context["food_val"] = last_food
    #     context["sleep_val"] = sleep_time
    #
    #     context["uploaded"] = 1
    #     if(age == ""):
    #         context["empty"] = 0
    #     elif(weight == ""):
    #         context["empty"] = 2
    #     elif(height == ""):
    #         context["empty"] = 3
    #     elif(last_food == ""):
    #         context["empty"] = 5
    #     elif(sleep_time == ""):
    #         context["empty"] = 6
    #     else:
    #         cleaned_height = height.strip('c').strip('m')
    #         if(gender == "male"):
    #             energy = (66.5 + 13.8 * int(weight) + 5 * int(cleaned_height)) - (6.8 * int(age))
    #         else:
    #             energy = (655.1 + 9.6 * int(weight) + 1.9 * int(cleaned_height)) - (4.7 * int(age))
    #
    #         if(activeness == "rendah"):
    #             energy *= 1.2
    #         elif(activeness == "sedang"):
    #             energy *= 1.3
    #         else:
    #             energy *= 1.5
    #
    #         context = {
    #             "empty": 0,
    #             "uploaded" : 0,
    #             "age_val":"",
    #             "weight_val":"",
    #             "height_val":"",
    #             "food_val":"",
    #             "sleep_val":"",
    #         }
    #
    #         context["energy"] = energy
    context = {}
    context["energy"] = request.session.get('post')

    return render(request, 'result.html',context)
