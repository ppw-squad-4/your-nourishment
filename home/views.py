
from django.shortcuts import render,redirect
from .models import Testimoni
from .forms import TestimoniForm

from django.shortcuts import render


# Create your views here.

def homepage(request):
    return render(request, 'homepage.html')

def base(request):
    return render(request, 'base.html')


# def create_testi(request):
#     return render(request, 'create_testi.html')

def create_testi(request):
	if request.method == "POST":
		form = TestimoniForm(request.POST)
		if form.is_valid():
			hasil = Testimoni(nama = form.data['nama'], usia = form.data['usia'], pesan = form.data['pesan'])
			hasil.save()
		return redirect("home:testi")
	else :
		form = TestimoniForm()
	return render(request, 'create_testi.html', {'form':form})
	# if request.method == "POST":
	# 	forms = form.TestimoniForm(request.POST)
	# 	if forms.is_valid():
	# 		nama = forms.cleaned_data["nama"]
	# 		usia = forms.cleaned_data["usia"]
	# 		pesan = forms.cleaned_data["pesan"]
	# 		testimoni = Testimoni.objects.create(
	# 			nama=nama,
	# 			usia= usia,
	# 			pesan=pesan,
	# 			)
	# 		testimoni.save()
	# 		return redirect("home:create_testi")
	# forms = form.TestimoniForm()
	# data = Testimoni.objects.all()
	# return render(request, 'create_testi.html', {
	# 	"form": forms,
	# 	"data": data
	# 	}
	# )

def testi(request):
	data = Testimoni.objects.all()
	return render(request, 'testi.html', {"data": data})
