from django.test import TestCase,Client
from django.urls import resolve
from .views import *
# Create your tests here.
class uniTest(TestCase):
    def test_ada_url(self):
        response = Client().get("/activity/")
        self.assertEqual(response.status_code,200)

    def test_ada_fungsi(self):
        response = resolve("/activity/")
        self.assertEqual(response.func,index)

    def test_ada_template(self):
        response = Client().get("/activity/")
        self.assertTemplateUsed(response,"activity.html")
