from django.urls import path
from . import views

app_name = "activity"

urlpatterns = [
    path('activity/', views.index, name='index'),
    path('result/',views.result, name = 'result'),
]
