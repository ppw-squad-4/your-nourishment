from django.test import TestCase, Client
from .models import Testimoni


class TestimoniUnitTest (TestCase):
	def test_testimoni_url_exists(self):
		response = Client().get('/testi/')
		self.assertEqual(response.status_code, 200)

	def test_model_testimoni(self):
		Testimoni.objects.create(nama="Pewe", usia=19, pesan="samlekom")
		hitung = Testimoni.objects.all().count()
		self.assertEqual(hitung, 1)

	def test_html_template(self):
		response = Client().get('/testi/')
		self.assertTemplateUsed(response, 'testi.html')


# Create your tests here.
