from django.urls import path
from . import views

app_name = 'blog'

urlpatterns = [
    path('blog/',views.blog, name='blog'),
    path('blog/id1/',views.article1, name='article1'),
    path('blog/id2/',views.article2, name='article2'),
    path('blog/id3/',views.article3, name='article3'),
    
] 
